let margin, h1pos;
$( document ).ready(function() {
    h1pos = $('h1').offset().top;
    margin = 200;
    $(document).on("scroll", onScroll);
    $('a[href*="#"]').click(function(event) {
        const currLink = $(this);
        let  hrefName= $.attr(this, 'href');
        if( (typeof (hrefName) !=='undefined') && (hrefName !== '#') ){
            if((hrefName!== '#carouselExampleIndicators') && ( hrefName.indexOf('pills') === -1) )  {
                const splitHref = hrefName.split('_');
                let tabName = null;
                if (splitHref.length>1) {
                    hrefName = splitHref[0];
                    tabName = splitHref[1];
                }
                const pos = $( hrefName ).offset();
                $('html, body').animate({
                    scrollTop: pos.top
                }, 500, function(){
                    if(tabName){
                        $('#faq a[href="#pills-'+tabName+'"]').tab('show');
                        $('.navbar-nav a').removeClass('active');
                        $(currLink).addClass('active');
                    }
                   
                });
            }
        }
        event.preventDefault();
    });
    $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
        
        const name = e.target.innerText.toLowerCase();
        $('.navbar-nav a').removeClass('active');
        if( name!=='papers'){
            selectLink(name);
        }
    })
});

function onScroll(event){
    const scrollPos = $(document).scrollTop();
    $('.navbar-nav a').each(function () {        
        const currLink = $(this);
        // console.log(currLink);
        let refElement = $(currLink.attr('href'));
        let faqSec = null;
        if( currLink.attr("href").indexOf('faq') !== -1) {
            refElement = $('#faq');
            faqSec = true;
        }
        if (refElement.offset().top <= scrollPos + margin && 
            (refElement.offset().top + refElement.height()) > scrollPos + margin) {
                $('.navbar-nav a').removeClass('active');
            if (faqSec) {
                $('#pills-tab a').each(function () {
                    const selection = $(this).attr('href').split('-');
                    if( $(this).hasClass('active') && selection[1] !== 'papers'){
                        selectLink(selection[1])
                    }
                });
            } else {
                currLink.addClass('active');
            }
        }
    });
    if( scrollPos >= h1pos) {
        if(!$('nav').hasClass('b_colour')){
            $('nav').addClass('b_colour');
        }
    } else {
        $('nav').removeClass('b_colour');
    }
}
function selectLink(name) {
    $('.navbar-nav a[href="#faq_'+name+'"]').addClass('active');
}